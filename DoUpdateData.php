<?php
require_once('connect.php');

$result['message']="";

$productID = ($_POST['productID']);
$productName = $_POST['productName'];
$productDescription = $_POST['productDescription'];
$productPrice = ($_POST['productPrice']);
if ($productID==""){
    $result["message"]="product ID must be filled !";
}else if($_POST['productName']==""){
    $result["message"]="product Name must be filled !";
}else if($productDescription==""){
    $result['message']=" desc pas null";

}else if ($productPrice==""){
    $result['message']=" price must be filled";

}else {
    try {
    $req = $dbh->prepare("UPDATE product set productName=:productName, productDescription=:productDescription, productPrice=:productPrice WHERE productID=:productID");
    $req->execute(array(
             ":productID" => $productID,
            ":productName" => $productName,
            ":productDescription" => $productDescription,
            ":productPrice" => $productPrice
            ));
        } catch (PDOException $e) {
            $result['message']= $e->getMessage();
        }
    if($req){
        $result['message']="Update succed";
    }else
    {
        $result['message']="Update failed";

    }
}

echo json_encode($result);
