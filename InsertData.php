<?php
require_once('connect.php');

$result['message']="";

$productID = htmlentities($_POST['productID']);
$productName = $_POST['productName'];
$productDescription = $_POST['productDescription'];
$productPrice = htmlentities($_POST['productPrice']);
if ($productID==""){
    $result["message"]="product ID must be filled !";
}else if($_POST['productName']==""){
    $result["message"]="product Name must be filled !";
}else if($productDescription==""){
    $result['message']=" desc pas null";

}else if ($productPrice==""){
    $result['message']=" price must be filled";

}else {
    try {
    $req = $dbh->prepare("INSERT INTO product (productName, productDescription, productPrice) VALUES (:productName, :productDescription, :productPrice)");
    $req->execute(array(
            ":productName" => $productName,
            ":productDescription" => $productDescription,
            ":productPrice" => $productPrice
            ));
        } catch (PDOException $e) {
            $result['message']= $e->getMessage();
        }
    if($req){
        $result['message']="Inserted succed";
    }else
    {
        $result['message']="Inserted failed";

    }
}

echo json_encode($result);
